/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package lab4.ejb;

import lab4.ebj.DataAccessBeanRemote;
import java.util.Date;
import javax.ejb.Stateless;

/**
 *
 * @author baro
 */
@Stateless
public class DataAccessBean implements DataAccessBeanRemote {

    private String OstatnieDane;
    private Date OstatniDostep;
    
    @Override
    public String getOstatnieDane(){
        return OstatniDostep + "\n" + OstatnieDane;
    }
    
    @Override
    public void setOstatnieDane(String dane, Date data){
        this.OstatnieDane = dane;
        this.OstatniDostep = data;
    }
}
